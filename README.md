# Ansible 

## step 1 - Installation and overview

To install ansible

* https://www.tecmint.com/sshpass-non-interactive-ssh-login-shell-script-ssh-password/
* https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-ansible-on-slackware-linux eh muito!

```
# pip3 install --upgrade pip
# pip3 install ansible
```

## step 2 - conhecendo os modulos 

```
$ ansible localhost -m setup
$ ansible localhost -m shell -a "df -h"
$ ansible localhost -m command -a "uptime"
$ ansible-doc -l | wc -l
$ ansible-doc -l | less
$ ansible-doc proxmox_kvm
```

## ansible.cfg e inventory

```
$ find / -name ansible.cfg 
$ vim ansible.cfg
$ vim inventory
```

## step 3 - tasks e playbooks

### playbook 




###

$ ansible-doc -l | grep netbox

https://github.com/netbox-community/ansible_modules


### nao falaremos

Nao falaremos de

* galaxy
* ansible-vault
* utilizar ansible via api python
* ansible tower ou ansible AWX
